var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  port: "3306",
  database: "nodejs"
});

con.connect(function (err){
    if(err) throw err;
    console.log('sudah terkoneksi');
});

module.exports = con;